<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jugadores extends Model
{
    protected $table = 'jugadores';

    public function tipos(){
        return $this->hasOne('App\TiposJugadores','id','tipo');
    }
}
