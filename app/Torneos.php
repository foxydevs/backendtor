<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Torneos extends Model
{
    protected $table = 'torneos';

    public function tipos(){
        return $this->hasOne('App\TiposTorneos','id','tipo');
    }
}
