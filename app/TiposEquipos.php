<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TiposEquipos extends Model
{
    protected $table = 'tipo_equipos';
}
