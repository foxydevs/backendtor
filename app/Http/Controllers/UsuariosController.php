<?php

namespace App\Http\Controllers;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Mail;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Usuarios;
use Response;
use Validator;
use Auth;
use Hash;
use DB;
use Storage;
use Faker\Factory as Faker;

class UsuariosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Response::json(Usuarios::with('tipos','roles','arbitros','equipos','ligas')->get(), 200);
    }

    public function login(Request $request) {
        $validator = Validator::make($request->all(), [
            'username'  => 'required',
            'password'  => 'required'
        ]);

        if ( $validator->fails() ) {
            $returnData = array (
                'status' => 400,
                'message' => 'Invalid Parameters',
                'validator' => $validator
            );
            return Response::json($returnData, 400);
        }
        else {
            try {

                $userdata = array(
                    'username'  => $request->get('username'),
                    'password'  => $request->get('password')
                );

                if(Auth::attempt($userdata, true)) {
                    $user = Usuarios::with('tipos','roles','arbitros','equipos','ligas')->find(Auth::user()->id);
                    $user->save();
                    
                    return Response::json($user, 200);
                }
                else {
                    $returnData = array (
                        'status' => 401,
                        'message' => 'No valid Username or Password'
                    );
                    return Response::json($returnData, 401);
                }

                return Response::json($newObject, 200);

            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'username'          => 'required',
            'password'          => 'required',
            'email'             => 'required'
        ]);
        if ( $validator->fails() ) {
            $returnData = array (
                'status' => 400,
                'message' => 'Invalid Parameters',
                'validator' => $validator
            );
            return Response::json($returnData, 400);
        }
        else {
            $email = $request->get('email');
            $email_exists  = Usuarios::whereRaw("email = ?", $email)->count();
            $user = $request->get('username');
            $user_exists  = Usuarios::whereRaw("username = ?", $user)->count();
            if($email_exists == 0 && $user_exists == 0){
                try {
                    DB::beginTransaction();
                    $newObject = new Usuarios();
                    $newObject->username          = $request->get('username');
                    $newObject->password          = Hash::make($request->get('password'));
                    $newObject->email             = $request->get('email');
                    $newObject->picture           = $request->get('picture','https://d30y9cdsu7xlg0.cloudfront.net/png/17241-200.png');
                    $newObject->privileges        = $request->get('privileges');
                    $newObject->tipo              = $request->get('tipo');
                    $newObject->rol               = $request->get('rol');
                    $newObject->liga              = $request->get('liga');
                    $newObject->equipo            = $request->get('equipo');
                    $newObject->arbitro           = $request->get('arbitro');
                    $newObject->estado            = 21;
                    $newObject->save();
                    Mail::send('emails.confirm', ['empresa' => 'FoxyLabs', 'url' => 'https://foxylabs.gt', 'app' => 'http://erpfoxy.foxylabs.xyz', 'password' => $request->get('password'), 'username' => $newObject->username, 'email' => $newObject->email, 'name' => "",], function (Message $message) use ($newObject){
                        $message->from('info@foxylabs.gt', 'Info FoxyLabs')
                                ->sender('info@foxylabs.gt', 'Info FoxyLabs')
                                ->to($newObject->email, "")
                                ->replyTo('info@foxylabs.gt', 'Info FoxyLabs')
                                ->subject('Usuario Creado');
                    
                    });
                    DB::commit();
                    return Response::json($newObject, 200);
                
                } catch (\Illuminate\Database\QueryException $e) {
                    if($e->errorInfo[0] == '01000'){
                        $errorMessage = "Error Constraint";
                    }  else {
                        $errorMessage = $e->getMessage();
                    }
                    DB::rollback();
                    $returnData = array (
                        'status' => 505,
                        'SQLState' => $e->errorInfo[0],
                        'message' => $errorMessage
                    );
                    return Response::json($returnData, 500);
                } catch (Exception $e) {
                    DB::rollback();
                    $returnData = array (
                        'status' => 500,
                        'message' => $e->getMessage()
                    );
                    return Response::json($returnData, 500);
                }
            } else {
                DB::rollback();
                $returnData = array(
                    'status' => 400,
                    'message' => 'User already exists',
                    'validator' => $validator->messages()->toJson()
                );
                return Response::json($returnData, 400);
            }
        }
    }
    public function recoveryPassword(Request $request){
        $objectUpdate = Usuarios::whereRaw('email=? or username=?',[$request->get('username'),$request->get('username')])->first();
        if ($objectUpdate) {
            try {
                DB::beginTransaction();
                $faker = Faker::create();
                $pass = $faker->password();
                $objectUpdate->password = bcrypt($pass);
                $objectUpdate->estado = 21;
                Mail::send('emails.recovery', ['empresa' => 'FoxyLabs', 'url' => 'https://foxylabs.gt', 'password' => $pass, 'email' => $objectUpdate->email, 'name' => "",], function (Message $message) use ($objectUpdate){
                    $message->from('info@foxylabs.gt', 'Info FoxyLabs')
                            ->sender('info@foxylabs.gt', 'Info FoxyLabs')
                            ->to($objectUpdate->email, "")
                            ->replyTo('info@foxylabs.gt', 'Info FoxyLabs')
                            ->subject('Contraseña Reestablecida');
                
                });
                $objectUpdate->save();
                DB::commit();
                return Response::json($objectUpdate, 200);
            } catch (Exception $e) {
                DB::rollback();
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            DB::rollback();
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    public function changePassword(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'new_pass' => 'required|min:3',//|regex:/^.*(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!-,:-@]).*$/
            'old_pass'      => 'required'
        ]);

        if ($validator->fails()) {
            $returnData = array(
                'status' => 400,
                'message' => 'Invalid Parameters',
                'validator' => $validator->messages()->toJson()
            );
            return Response::json($returnData, 400);
        }
        else {
            $old_pass = $request->get('old_pass');
            $new_pass_rep = $request->get('new_pass_rep');
            $new_pass =$request->get('new_pass');
            $objectUpdate = Usuarios::find($id);
            if ($objectUpdate) {
                try {
                    if(Hash::check($old_pass, $objectUpdate->password))
                    {                       
                        if($new_pass_rep != $new_pass)
                        {
                            $returnData = array(
                                'status' => 404,
                                'message' => 'Passwords do not match'
                            );
                            return Response::json($returnData, 404);
                        }

                        if($old_pass == $new_pass)
                        {
                            $returnData = array(
                                'status' => 404,
                                'message' => 'New passwords it is same the old password'
                            );
                            return Response::json($returnData, 404);
                        }
                        $objectUpdate->password = Hash::make($new_pass);
                        $objectUpdate->estado = 1;
                        $objectUpdate->save();

                        return Response::json($objectUpdate, 200);
                    }else{
                        $returnData = array(
                            'status' => 404,
                            'message' => 'Invalid Password'
                        );
                        return Response::json($returnData, 404);
                    }
                }
                catch (Exception $e) {
                    $returnData = array(
                        'status' => 500,
                        'message' => $e->getMessage()
                    );
                }
            }
            else {
                $returnData = array(
                    'status' => 404,
                    'message' => 'No record found'
                );
                return Response::json($returnData, 404);
            }
        }
    }
    public function uploadAvatar(Request $request, $id) {
        $objectUpdate = Usuarios::find($id);
        if ($objectUpdate) {

            $validator = Validator::make($request->all(), [
                'avatar'      => 'required|image|mimes:jpeg,png,jpg'
            ]);

            if ($validator->fails()) {
                $returnData = array(
                    'status' => 400,
                    'message' => 'Invalid Parameters',
                    'validator' => $validator->messages()->toJson()
                );
                return Response::json($returnData, 400);
            }
            else {
                try {
                    $path = Storage::disk('s3')->put('avatars', $request->avatar);

                    $objectUpdate->picture = Storage::disk('s3')->url($path);
                    $objectUpdate->save();

                    return Response::json($objectUpdate, 200);
                }
                catch (Exception $e) {
                    $returnData = array(
                        'status' => 500,
                        'message' => $e->getMessage()
                    );
                }

            }

            return Response::json($objectUpdate, 200);
        }
        else {
            $returnData = array(
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $objectSee = Usuarios::where('id','=',$id)->with('tipos','roles')->first();
        if ($objectSee) {

            return Response::json($objectSee, 200);
        
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $objectUpdate = Usuarios::find($id);
        if ($objectUpdate) {
            try {
                $objectUpdate->username          = $request->get('username', $objectUpdate->username);
                $objectUpdate->email             = $request->get('email', $objectUpdate->email);
                $objectUpdate->picture           = $request->get('picture', $objectUpdate->picture);
                $objectUpdate->privileges        = $request->get('privileges', $objectUpdate->privileges);
                $objectUpdate->rol               = $request->get('rol', $objectUpdate->rol);
                $objectUpdate->liga              = $request->get('liga', $objectUpdate->liga);
                $objectUpdate->tipo              = $request->get('tipo', $objectUpdate->tipo);
                $objectUpdate->equipo            = $request->get('equipo', $objectUpdate->equipo);
                $objectUpdate->arbitro           = $request->get('arbitro', $objectUpdate->arbitro);
                $objectUpdate->estado           = $request->get('estado', $objectUpdate->estado);
                $objectUpdate->admin           = $request->get('admin', $objectUpdate->admin);
                $objectUpdate->liga_id           = $request->get('liga_id', $objectUpdate->liga_id);
                $objectUpdate->save();
                return Response::json($objectUpdate, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $objectDelete = Usuarios::find($id);
        if ($objectDelete) {
            try {
                Usuarios::destroy($id);
                return Response::json($objectDelete, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
}
