<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Equipos;
use App\EquiposPartido;
use App\JugadorEquipo;
use App\Jugadores;
use Response;
use Validator;
class EquiposController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Response::json(Equipos::with('tipos')->get(), 200);
    }

    public function getEquiposByLiga($id)
    {
        $objectSee = Equipos::where('liga','=',$id)->with('torneos')->get();
        if ($objectSee) {
            
            return Response::json($objectSee, 200);
        
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    public function getEquiposBussy()
    {
        $objectSee = JugadorEquipo::select('equipo')->get();
        if ($objectSee) {

            $objectRet = Equipos::whereIn('id',$objectSee)->with('torneos')->get();

            return Response::json($objectRet, 200);
        
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    public function getEquiposBussyLiga($id)
    {
        $objectSee = JugadorEquipo::select('equipo')->get();
        if ($objectSee) {

            $objectRet = Equipos::whereIn('id',$objectSee)->where('liga','=',$id)->with('torneos')->get();

            return Response::json($objectRet, 200);
        
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    public function getEquiposByTipo($id)
    {
        $objectSee = Equipos::whereRaw('tipo=?',[$id])->with('torneos')->get();
        if ($objectSee) {


            return Response::json($objectSee, 200);
        
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    public function getEquiposByGrupo($id)
    {
        $objectSee = EquiposPartido::select('equipo')->whereRaw('grupo=?',$id)->get();
        if ($objectSee) {

            $objectRet = Equipos::whereIn('id',$objectSee)->with('torneos')->get();

            return Response::json($objectRet, 200);
        
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nombre'          => 'required',
            'tipo'            => 'required'
        ]);
        if ( $validator->fails() ) {
            $returnData = array (
                'status' => 400,
                'message' => 'Invalid Parameters',
                'validator' => $validator
            );
            return Response::json($returnData, 400);
        }
        else {
            try {
                $newObject = new Equipos();
                $newObject->nombre            = $request->get('nombre');
                $newObject->descripcion       = $request->get('descripcion');
                $newObject->tipo              = $request->get('tipo');
                $newObject->liga              = $request->get('liga');
                $newObject->picture              = $request->get('picture','https://d30y9cdsu7xlg0.cloudfront.net/png/17241-200.png');
                $newObject->save();
                return Response::json($newObject, 200);
            
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
    }
    public function getJugadores($id)
    {
        $objectSee = Equipos::find($id);
        if ($objectSee) {
            
            $student = JugadorEquipo::select('jugador')->where('equipo',$objectSee->id)->get();
            $students = Jugadores::whereIn('id',$student)->with('tipos')->get();
            return Response::json($students, 200);
        
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    public function getJugadoresFree($id)
    {
        $objectSee = Equipos::find($id);
        if ($objectSee) {
            
            $student = JugadorEquipo::where('equipo',$objectSee->id)->whereRaw('estado<=1')->with('jugadores')->get();
            // $students = Jugadores::whereIn('id',$student)->get();
            return Response::json($student, 200);
        
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    public function getJugadoresTitular($id)
    {
        $objectSee = Equipos::find($id);
        if ($objectSee) {
            
            $student = JugadorEquipo::where('equipo',$objectSee->id)->whereRaw('estado>1')->with('jugadores')->get();
            // $students = Jugadores::whereIn('id',$student)->get();
            return Response::json($student, 200);
        
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $objectSee = Equipos::whereRaw('id=?',$id)->with('jugadores')->first();
        if ($objectSee) {
            
            return Response::json($objectSee, 200);
        
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    public function update(Request $request, $id)
    {
        $objectUpdate = Equipos::find($id);
        if ($objectUpdate) {
            try {
                $objectUpdate->nombre      = $request->get('nombre', $objectUpdate->nombre);
                $objectUpdate->descripcion = $request->get('descripcion', $objectUpdate->descripcion);
                $objectUpdate->estado      = $request->get('estado', $objectUpdate->estado);
                $objectUpdate->tipo        = $request->get('tipo', $objectUpdate->tipo);
                $objectUpdate->picture        = $request->get('picture', $objectUpdate->picture);
                $objectUpdate->save();
                return Response::json($objectUpdate, 200);
            }catch (\Illuminate\Database\QueryException $e) {
                if($e->errorInfo[0] == '01000'){
                    $errorMessage = "Error Constraint";
                }  else {
                    $errorMessage = $e->getMessage();
                }
                $returnData = array (
                    'status' => 505,
                    'SQLState' => $e->errorInfo[0],
                    'message' => $errorMessage
                );
                return Response::json($returnData, 500);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    public function uploadAvatar(Request $request, $id) {
        $objectUpdate = Equipos::find($id);
        if ($objectUpdate) {

            $validator = Validator::make($request->all(), [
                'avatar'      => 'required|image|mimes:jpeg,png,jpg'
            ]);

            if ($validator->fails()) {
                $returnData = array(
                    'status' => 400,
                    'message' => 'Invalid Parameters',
                    'validator' => $validator->messages()->toJson()
                );
                return Response::json($returnData, 400);
            }
            else {
                try {
                    $path = Storage::disk('s3')->put('equipos', $request->avatar);

                    $objectUpdate->picture = Storage::disk('s3')->url($path);
                    $objectUpdate->save();

                    return Response::json($objectUpdate, 200);
                }
                catch (Exception $e) {
                    $returnData = array(
                        'status' => 500,
                        'message' => $e->getMessage()
                    );
                }

            }

            return Response::json($objectUpdate, 200);
        }
        else {
            $returnData = array(
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $objectDelete = Equipos::find($id);
        if ($objectDelete) {
            try {
                Equipos::destroy($id);
                return Response::json($objectDelete, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
}
