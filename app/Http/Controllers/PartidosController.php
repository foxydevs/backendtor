<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Partidos;
use App\EquiposPartido;
use App\TorneosLiga;
use App\Torneos;
use App\Goles;
use App\Tarjetas;
use Response;
use Validator;
use DB;

class PartidosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Response::json(Partidos::with('equipos','torneos','tipos','grupos')->get(), 200);
    }

    public function getPartidosByLiga($id)
    {
        $objectSee1 = TorneosLiga::select('torneo')->whereRaw('liga=?',[$id])->get();
        if ($objectSee1) {
            $objectSee2 = Torneos::whereIn('id',$objectSee1)->get();
            $objectSee = Partidos::whereIn('torneo',$objectSee2)->with('equipos','torneos','tipos','grupos')->get();
            
            return Response::json($objectSee, 200);
        
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    public function getPartidosByEquipo($id)
    {
        $objectSee1 = EquiposPartido::select('partido')->whereRaw('equipo=?',[$id])->get();
        if ($objectSee1) {
            $objectSee = Partidos::whereIn('id',$objectSee1)->with('equipos','torneos','tipos','grupos')->get();
            
            return Response::json($objectSee, 200);
        
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    public function getPartidosByTorneo($id)
    {
        $objectSee = Partidos::whereRaw('torneo=?',$id)->with('equipos','torneos','tipos','grupos')->get();
        if ($objectSee) {
            
            return Response::json($objectSee, 200);
        
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    public function getTorneosByEquipo($id)
    {
        $objectSee1 = EquiposPartido::select('partido')->whereRaw('equipo=?',[$id])->get();
        if ($objectSee1) {
            $objectSee = Partidos::whereIn('id',$objectSee1)->with('equipos','torneos','tipos','grupos')->groupby('torneo')->get();
            

            return Response::json($objectSee, 200);
        
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    public function getGolesByPartidos($id)
    {
        $objectSee1 = Goles::whereRaw('partido=?',[$id])->get();
        if ($objectSee1) {
            
            return Response::json($objectSee1, 200);
        
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    public function getTarjetasByPartidos($id)
    {
        $objectSee1 = Tarjetas::whereRaw('partido=?',[$id])->get();
        if ($objectSee1) {
            
            return Response::json($objectSee1, 200);
        
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    public function getPartiosByGrupo($id)
    {
        $objectSee = EquiposPartido::select('partido')->whereRaw('grupo=?',$id)->get();
        if ($objectSee) {

            $objectRet = Partidos::whereIn('id',$objectSee)->get();

            return Response::json($objectRet, 200);
        
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'fecha'          => 'required',
            'torneo'         => 'required',
            'tipo'           => 'required'
        ]);
        if ( $validator->fails() ) {
            $returnData = array (
                'status' => 400,
                'message' => 'Invalid Parameters',
                'validator' => $validator
            );
            return Response::json($returnData, 400);
        }
        else {
            try {
                DB::beginTransaction();
                $newObject = new Partidos();
                $newObject->fecha            = $request->get('fecha',null);
                $newObject->hora             = $request->get('hora',null);
                $newObject->torneo           = $request->get('torneo',null);
                $newObject->tipo             = $request->get('tipo',null);
                $newObject->titulo             = $request->get('titulo',null);
                $newObject->descripcion             = $request->get('descripcion',null);
                $newObject->grupo             = $request->get('grupo',null);
                $newObject->save();
                if ( $request->get('equipos') )
                {
                    $Array = $request->get('equipos');
                    $partido = $newObject->id;
                    foreach ($Array as $value)
                    {
                        $existe = EquiposPartido::whereRaw('equipo=? and partido=?',[$value['id'],$partido])->first();
                        if(!($existe)){    
                            $newObjectPartido = new EquiposPartido();
                            $newObjectPartido->equipo           = $value['id'];
                            $newObjectPartido->partido          = $partido;
                            $newObjectPartido->grupo          = $request->get('grupo',null);
                            $newObjectPartido->save();
                        }
                    }
                    DB::commit();
                    $returnData = array (
                        'status' => 200,
                        'message' => "success"
                    );
                    return Response::json($returnData, 200);
                }
                DB::commit();
                $newObject->equipos;
                return Response::json($newObject, 200);
            
            } catch (\Illuminate\Database\QueryException $e) {
                DB::rollback();
                if($e->errorInfo[0] == '01000'){
                    $errorMessage = "Error Constraint";
                }  else {
                    $errorMessage = $e->getMessage();
                }
                $returnData = array (
                    'status' => 505,
                    'SQLState' => $e->errorInfo[0],
                    'message' => $errorMessage
                );
                return Response::json($returnData, 500);
            } catch (Exception $e) {
                DB::rollback();
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $objectSee = Partidos::where('id','=',$id)->with('equipos','torneos','tipos','grupos')->first();
        if ($objectSee) {
            
            return Response::json($objectSee, 200);
        
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }


    public function getJugadores($id, $id2)
    {
        $objectSee = Equipos::find($id);
        if ($objectSee) {
            
            $student = JugadorEquipo::select('jugador')->where('equipo',$objectSee->id)->get();
            $students = Jugadores::whereIn('id',$student)->get();
            return Response::json($students, 200);
        
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $objectUpdate = Partidos::find($id);
        if ($objectUpdate) {
            try {
                DB::beginTransaction();
                $objectUpdate->fecha            = $request->get('fecha', $objectUpdate->fecha);
                $objectUpdate->hora             = $request->get('hora', $objectUpdate->hora);
                $objectUpdate->torneo           = $request->get('torneo', $objectUpdate->torneo);
                $objectUpdate->tipo             = $request->get('tipo', $objectUpdate->tipo);
                $objectUpdate->estado           = $request->get('estado', $objectUpdate->estado);
                $objectUpdate->titulo           = $request->get('titulo', $objectUpdate->titulo);
                $objectUpdate->grupo           = $request->get('grupo', $objectUpdate->grupo);
                $objectUpdate->descripcion           = $request->get('descripcion', $objectUpdate->descripcion);
                $objectUpdate->save();
                if ( $request->get('equipos') )
                {
                    $partido = $objectUpdate->id;
                    $finder = EquiposPartido::whereRaw('partido=?',[$partido])->get();
                    foreach ($finder as $value)
                    {
                        $objectDelete = EquiposPartido::find($value['id']);
                        if ($objectDelete) {
                            try {
                                EquiposPartido::destroy($value['id']);
                            } catch (Exception $e) {
                                $returnData = array (
                                    'status' => 500,
                                    'message' => $e->getMessage()
                                );
                                return Response::json($returnData, 500);
                            }
                        }
                    }
                    $Array = $request->get('equipos');
                    foreach ($Array as $value)
                    {
                        $existe = EquiposPartido::whereRaw('equipo=? and partido=?',[$value['id'],$partido])->first();
                        if(!($existe)){    
                            $newObjectPartido = new EquiposPartido();
                            $newObjectPartido->equipo           = $value['id'];
                            $newObjectPartido->partido          = $partido;
                            $newObjectPartido->grupo          = $request->get('grupo',null);
                            $newObjectPartido->save();
                        }
                    }
                    DB::commit();
                    $returnData = array (
                        'status' => 200,
                        'message' => "success"
                    );
                    return Response::json($returnData, 200);
                }
                DB::commit();
                $objectUpdate->equipos;
                
                if ( $request->get('grupo') ){
                    $partido = $objectUpdate->id;
                        $objectUpdate1 = EquiposPartido::whereRaw('partido=?',[$partido]);
                    if ($objectUpdate1) {
                        try {
                            // $objectUpdate1->grupo = $request->get('grupo', $objectUpdate1->grupo);
                    
                            // $objectUpdate1->save();
                            return Response::json($objectUpdate1, 200);

                            $objectUpdate->grupo = $objectUpdate1->grupo;
                        } catch (Exception $e) {
                            $returnData = array (
                                'status' => 500,
                                'message' => $e->getMessage()
                            );
                            return Response::json($returnData, 500);
                        }
                    }
                    else {
                        $returnData = array (
                            'status' => 404,
                            'message' => 'No record found'
                        );
                        return Response::json($returnData, 404);
                    }
                }

                return Response::json($objectUpdate, 200);
            
            } catch (\Illuminate\Database\QueryException $e) {
                if($e->errorInfo[0] == '01000'){
                    $errorMessage = "Error Constraint";
                }  else {
                    $errorMessage = $e->getMessage();
                }
                $returnData = array (
                    'status' => 505,
                    'SQLState' => $e->errorInfo[0],
                    'message' => $errorMessage
                );
                return Response::json($returnData, 500);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $objectDelete = Partidos::find($id);
        if ($objectDelete) {
            try {
                Partidos::destroy($id);
                return Response::json($objectDelete, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
}
