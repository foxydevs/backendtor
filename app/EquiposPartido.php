<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EquiposPartido extends Model
{
    protected $table = 'equipos_partido';

    public function equipos(){
        return $this->hasOne('App\Equipos','id','equipo');
    }
}
