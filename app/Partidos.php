<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Partidos extends Model
{
    protected $table = 'partidos';

    public function equipos(){
        return $this->hasMany('App\EquiposPartido','partido','id')->with('equipos');
    }

    public function torneos(){
        return $this->hasOne('App\Torneos','id','torneo')->with('tipos');
    }

    public function tipos(){
        return $this->hasOne('App\TiposPartidos','id','tipo');
    }

    public function grupos(){
        return $this->hasOne('App\Grupos','id','grupo');
    }
}
