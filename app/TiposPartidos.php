<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TiposPartidos extends Model
{
    protected $table = 'tipo_partidos';
}
