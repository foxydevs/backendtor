<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NoticiasComentarios extends Model
{
    protected $table = 'noticias_comentarios';

    public function usuarios(){
        return $this->hasOne('App\Usuarios','id','usuario');
    }

    public function commentNoticias() {
        return $this->hasOne('App\Noticias', 'id', 'noticia');
    }

}
