<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TiposJugadores extends Model
{
    protected $table = 'tipo_jugadores';
}
