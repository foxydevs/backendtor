<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Noticias extends Model
{
    protected $table = 'noticias';

    public function usuarios(){
        return $this->hasOne('App\Usuarios','id','usuario');
    }

    public function comments() {
        return $this->hasMany('App\NoticiasComentarios', 'noticia');
    }
}
