<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Grupos extends Model
{
    protected $table = 'grupos';

    public function torneos(){
        return $this->hasOne('App\Torneos','id','torneo')->with('tipos');
    }
}
