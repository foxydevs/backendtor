<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Equipos extends Model
{
    protected $table = 'equipos';

    public function tipos(){
        return $this->hasOne('App\TiposEquipos','id','tipo');
    }

    public function torneos(){
        return $this->hasOne('App\TiposTorneos','id','tipo');
    }

    public function jugadores(){
        return $this->hasMany('App\JugadorEquipo','equipo','id')->with('jugadores');
    }
}
