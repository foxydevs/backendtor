<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Usuarios extends Authenticatable
{
    protected $table = 'usuarios';

    protected $hidden = array('password','remember_token');
    
    public function roles(){
        return $this->hasOne('App\Roles','id','rol');
    }

    public function tipos(){
        return $this->hasOne('App\TiposUsuarios','id','tipo');
    }

    public function arbitros(){
        return $this->hasOne('App\Arbitros','id','arbitro');
    }

    public function equipos(){
        return $this->hasOne('App\Equipos','id','equipo');
    }

    public function ligas(){
        return $this->hasOne('App\Ligas','id','liga');
    }
}
