<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JugadorEquipo extends Model
{
    protected $table = 'jugador_equipo';

    public function jugadores(){
        return $this->hasOne('App\Jugadores','id','jugador')->with('tipos');
    }
}
