<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TorneosLiga extends Model
{
    protected $table = 'torneos_liga';

    public function torneos(){
        return $this->hasOne('App\Torneos','id','torneo');
    }

    public function ligas(){
        return $this->hasOne('App\Ligas','id','liga');
    }
}
