<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::resource('accesos', 'AccesosController');
Route::resource('arbitraje', 'ArbitrajeController');
Route::resource('arbitros', 'ArbitrosController');
Route::resource('calificaciones', 'CalificacionesController');
Route::resource('equipos', 'EquiposController');
Route::resource('equipospartido', 'EquiposPartidoController');
Route::resource('goles', 'GolesController');
Route::resource('grupos', 'GruposController');
Route::resource('jugadorequipo', 'JugadorEquipoController');
Route::resource('jugadores', 'JugadoresController');
Route::resource('ligas', 'LigasController');
Route::resource('modulos', 'ModulosController');
Route::resource('noticias', 'NoticiasController');
Route::resource('noticiascomentarios', 'NoticiasComentariosController');
Route::resource('partidos', 'PartidosController');
Route::resource('penalizaciones', 'PenalizacionesController');
Route::resource('puntos', 'PuntosController');
Route::resource('roles', 'RolesController');
Route::resource('tarjetas', 'TarjetasController');
Route::resource('tipoequipos', 'TipoEquiposController');
Route::resource('tipojugadores', 'TipoJugadoresController');
Route::resource('tipopartidos', 'TipoPartidosController');
Route::resource('tipotorneos', 'TipoTorneosController');
Route::resource('tipousuarios', 'TipoUsuariosController');
Route::resource('torneos', 'TorneosController');
Route::resource('torneosliga', 'TorneosLigaController');
Route::resource('usuarios', 'UsuariosController');

Route::get('ligas/{id}/jugadores', 'JugadoresController@getJugadoresByLiga');
Route::get('ligas/{id}/equipos', 'EquiposController@getEquiposByLiga');
Route::get('ligas/{id}/arbitros', 'ArbitrosController@getArbitrosByLiga');
Route::get('ligas/{id}/torneo', 'TorneosController@getTorneosByLiga');
Route::get('ligas/{id}/partidos', 'PartidosController@getPartidosByLiga');
Route::get('ligas/{id}/grupos', 'GruposController@getGruposByLiga');
Route::get('partidos/{id}/goles', 'PartidosController@getGolesByPartidos');
Route::get('partidos/{id}/tarjetas', 'PartidosController@getTarjetasByPartidos');

Route::get('ligas/{id}/torneos', 'LigasController@getTorneos');
Route::get('equipos/{id}/jugadores', 'EquiposController@getJugadores');
Route::get('equipos/{id}/jugadores/free', 'EquiposController@getJugadoresFree');
Route::get('equipos/{id}/jugadores/titular', 'EquiposController@getJugadoresTitular');
Route::get('equipos/{id}/partidos', 'PartidosController@getPartidosByEquipo');
Route::get('torneos/{id}/partidos', 'PartidosController@getPartidosByTorneo');

Route::get('torneos/{id}/grupos', 'GruposController@getGruposByTorneo');
Route::get('grupos/equipos/{id}', 'EquiposController@getEquiposByGrupo');
Route::get('grupos/partidos/{id}', 'PartidosController@getPartiosByGrupo');

Route::get('equipos/{id}/torneos', 'PartidosController@getTorneosByEquipo');
Route::get('arbitros/{id}/partidos', 'ArbitrosController@getPartidos');
Route::get('arbitros/{id}/ligas', 'ArbitrosController@getLigas');
Route::get('arbitros/{id}/torneos', 'ArbitrosController@getTorneos');
Route::get('equipos/{id}/jugadores/{id2}', 'PartidosController@getJugadores');

Route::get('free/{id}/torneos', 'TorneosController@getFreeTorneosLiga');
Route::get('free/{id}/jugadores', 'JugadoresController@getFreeJugadoresLiga');
Route::get('bussy/{id}/equipos', 'EquiposController@getEquiposBussyLiga');
Route::get('tipo/{id}/equipos', 'EquiposController@getEquiposByTipo');

Route::get('free/torneos', 'TorneosController@getFreeTorneos');
Route::get('free/jugadores', 'JugadoresController@getFreeJugadores');
Route::get('bussy/equipos', 'EquiposController@getEquiposBussy');
Route::get('bussy/ligas', 'LigasController@getLigasBussy');

Route::post('torneosliga/signedup', 'TorneosLigaController@setTorneos');
Route::post('jugadorequipo/signedup', 'JugadorEquipoController@setJugadores');
Route::post('jugadorequipo/signedupdate', 'JugadorEquipoController@setJugadoresUpdate');
Route::post('arbitraje/signedup', 'ArbitrajeController@setPartidos');
Route::post('arbitraje/signeddown', 'ArbitrajeController@removePartidos');
Route::post('jugadorequipo/signeddown', 'JugadorEquipoController@removeJugadores');
Route::post('torneosliga/signeddown', 'TorneosLigaController@removeTorneos');

Route::get('usuarios/{id}/modulos', 'AccesosController@getAccesos');
Route::get('usuarios/{id}/modulos/{id2}', 'AccesosController@getAcceso');

Route::post('arbitros/{id}/upload', 'ArbitrosController@uploadAvatar');
Route::post('jugadores/{id}/upload', 'JugadoresController@uploadAvatar');
Route::post('equipos/{id}/upload', 'EquiposController@uploadAvatar');
Route::post('liga/{id}/upload', 'LigasController@uploadAvatar');
Route::post('usuarios/{id}/upload', 'UsuariosController@uploadAvatar');

Route::post('usuarios/{id}/changepassword', 'UsuariosController@changePassword');
Route::post('usuarios/password/reset', 'UsuariosController@recoveryPassword');

Route::post('login', 'UsuariosController@login');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
