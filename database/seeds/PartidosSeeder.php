<?php

use Illuminate\Database\Seeder;

class PartidosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tipo_partidos')->insert([
            'nombre'           => "Oficial",
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);
        DB::table('partidos')->insert([
            'fecha'            => "2018-02-20",
            'hora'             => "10:30:00",
            'torneo'           => 1,
            'tipo'             => 1,
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);

        DB::table('equipos_partido')->insert([
            'resultado'        => 0,
            'equipo'           => 1,
            'partido'          => 1,
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);

        DB::table('equipos_partido')->insert([
            'resultado'        => 0,
            'equipo'           => 2,
            'partido'          => 1,
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);
    }
}
