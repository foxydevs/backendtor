<?php

use Illuminate\Database\Seeder;

class PuntosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('puntos')->insert([
            'puntos'           => 3,
            'equipo'           => 1,
            'torneo'           => 1,
            'partido'          => 1,
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);
        DB::table('puntos')->insert([
            'puntos'           => 1,
            'equipo'           => 2,
            'torneo'           => 1,
            'partido'          => 1,
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);
    }
}
