<?php

use Illuminate\Database\Seeder;

class UsuariosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tipo_usuarios')->insert([
            'nombre'           => "Ligas",
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);
        DB::table('tipo_usuarios')->insert([
            'nombre'           => "Equipos",
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);
        DB::table('tipo_usuarios')->insert([
            'nombre'           => "Arbitros",
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);
        DB::table('tipo_usuarios')->insert([
            'nombre'           => "Administrador",
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);
        DB::table('roles')->insert([
            'nombre'           => "Administrador",
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);
        DB::table('roles')->insert([
            'nombre'           => "Usuario",
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);
        DB::table('usuarios')->insert([
            'username'         => "admin",
            'password'         => bcrypt("foxylabs"),
            'email'            => "info@foxylabs.gt",
            'rol'              => 1,
            'tipo'             => 4,
            'liga'             => null,
            'equipo'           => null,
            'arbitro'          => null,
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);
        DB::table('usuarios')->insert([
            'username'         => "fut7",
            'password'         => bcrypt("foxylabs"),
            'email'            => "f7@foxylabs.gt",
            'rol'              => 1,
            'liga'             => 1,
            'tipo'             => 1,
            'equipo'           => null,
            'arbitro'          => null,
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);
        DB::table('usuarios')->insert([
            'username'         => "arbitro",
            'password'         => bcrypt("foxylabs"),
            'email'            => "arbitro@foxylabs.gt",
            'rol'              => 1,
            'liga'             => null,
            'tipo'             => 3,
            'equipo'           => null,
            'arbitro'          => 1,
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);
        DB::table('usuarios')->insert([
            'username'         => "equipo",
            'password'         => bcrypt("foxylabs"),
            'email'            => "equipo@foxylabs.gt",
            'rol'              => 1,
            'liga'             => null,
            'tipo'             => 2,
            'equipo'           => 1,
            'arbitro'          => null,
            'deleted_at'       => null,
            'created_at'       => date('Y-m-d H:m:s'),
            'updated_at'       => date('Y-m-d H:m:s')
        ]);
    }
}
