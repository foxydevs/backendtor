<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMensajesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mensajes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('subject')->nullable()->default(null);
            $table->string('message')->nullable()->default(null);
            $table->string('picture')->nullable()->default(null);
            $table->integer('state')->default(1);
            $table->integer('tipo')->default(1);
            
            $table->integer('user_send')->nullable()->default(null)->unsigned();
            $table->foreign('user_send')->references('id')->on('usuarios')->onDelete('cascade');

            $table->integer('user_receipt')->nullable()->default(null)->unsigned();
            $table->foreign('user_receipt')->references('id')->on('usuarios')->onDelete('cascade');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mensajes');
    }
}
