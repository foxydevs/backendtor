<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePuntosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('puntos', function (Blueprint $table) {
            $table->increments('id');
            $table->Integer('puntos')->nullable()->default(0);
            $table->Integer('equipo')->unsigned()->nullable()->default(null);
            $table->foreign('equipo')->references('id')->on('equipos')->onDelete('cascade');
            $table->Integer('torneo')->unsigned()->nullable()->default(null);
            $table->foreign('torneo')->references('id')->on('torneos')->onDelete('cascade');
            $table->Integer('partido')->unsigned()->nullable()->default(null);
            $table->foreign('partido')->references('id')->on('partidos')->onDelete('cascade');
            $table->Integer('grupo')->unsigned()->nullable()->default(null);
            $table->foreign('grupo')->references('id')->on('grupos')->onDelete('cascade');
            $table->tinyInteger('estado')->nullable()->default(1);

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('puntos');
    }
}
