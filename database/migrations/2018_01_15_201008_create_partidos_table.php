<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePartidosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partidos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('titulo')->nullable()->default(null);
            $table->string('descripcion')->nullable()->default(null);
            $table->date('fecha')->nullable()->default(null);
            $table->time('hora')->nullable()->default(null);
            $table->tinyInteger('estado')->nullable()->default(1);
            
            $table->Integer('torneo')->unsigned()->nullable()->default(null);
            $table->foreign('torneo')->references('id')->on('torneos')->onDelete('cascade');
            $table->Integer('tipo')->unsigned()->nullable()->default(null);
            $table->foreign('tipo')->references('id')->on('tipo_partidos')->onDelete('cascade');
            $table->Integer('grupo')->unsigned()->nullable()->default(null);
            $table->foreign('grupo')->references('id')->on('grupos')->onDelete('cascade');
            $table->Integer('tipo_torneo')->unsigned()->nullable()->default(null);
            $table->foreign('tipo_torneo')->references('id')->on('tipo_torneos')->onDelete('cascade');
            
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('partidos');
    }
}
