<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('goles', function (Blueprint $table) {
            $table->increments('id');
            $table->Integer('cantidad')->nullable()->default(0);
            $table->Integer('jugador')->unsigned()->nullable()->default(null);
            $table->foreign('jugador')->references('id')->on('jugadores')->onDelete('cascade');
            $table->Integer('partido')->unsigned()->nullable()->default(null);
            $table->foreign('partido')->references('id')->on('partidos')->onDelete('cascade');
            $table->Integer('equipo')->unsigned()->nullable()->default(null);
            $table->foreign('equipo')->references('id')->on('equipos')->onDelete('cascade');
            $table->tinyInteger('estado')->nullable()->default(1);

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('goles');
    }
}
