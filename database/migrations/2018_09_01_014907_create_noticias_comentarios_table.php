<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNoticiasComentariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('noticias_comentarios', function (Blueprint $table) {
            $table->increments('id');
            $table->string('comment')->nullable()->default(null);
            $table->integer('state')->default(1);
            
            $table->integer('noticia')->nullable()->default(null)->unsigned();
            $table->foreign('noticia')->references('id')->on('noticias')->onDelete('cascade');

            $table->integer('user')->nullable()->default(null)->unsigned();
            $table->foreign('user')->references('id')->on('usuarios')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('noticias_comentarios');
    }
}
