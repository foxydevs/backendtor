<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePenalizacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('penalizaciones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('descripcion')->nullable()->default(null);
            $table->Integer('cantidad')->nullable()->default(0);
            $table->Integer('jugador')->unsigned()->nullable()->default(null);
            $table->foreign('jugador')->references('id')->on('jugadores')->onDelete('cascade');
            $table->Integer('torneo')->unsigned()->nullable()->default(null);
            $table->foreign('torneo')->references('id')->on('torneos')->onDelete('cascade');
            $table->tinyInteger('estado')->nullable()->default(1);

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('penalizaciones');
    }
}
